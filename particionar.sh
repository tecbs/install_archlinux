#!/bin/bash
DISK=""
BOOT_SIZE="512M"
SWAP_SIZE="1"
PASSWD_DISK="aaa"
LANG_TYPE="UTF-8"

select_disk() {
# Enumerar los discos existentes
available_disks=($(lsblk --noheadings --raw -d | awk '{print $1"("$4")"}'))

# Verificar si existen discos disponibles
if [ ${#available_disks[@]} -eq 0 ]; then
  echo "No se encontraron discos disponibles."
  exit 1
fi

# Agregar "Salir" como una opción adicional al final de la lista
available_disks+=("Exit/Salir")

# Mostrar un menú desplegable para seleccionar un disco
PS3="Por favor, elige un disco de la lista (o 'Salir' para salir): "
select selected_disk in "${available_disks[@]}"; do
  if [ "$selected_disk" = "Salir" ]; then
    echo "Saliendo del programa."
    exit 0
  elif [ -n "$selected_disk" ]; then
    DISK=$(echo "$selected_disk" | sed 's/([^)]*)//')
    echo "Has seleccionado el disco: $DISK"
    break
  else
    echo "Selección no válida. Por favor, elige un número de la lista o 'Salir' para salir."
  fi
done

}

check_uefi_mode() {
  [ -d "/sys/firmware/efi/efivars" ] && echo "y" || echo "n"
}



create_partitions() {

  sgdisk --zap-all /dev/${DISK}

  parted /dev/${DISK} mklabel gpt
  parted /dev/${DISK} mkpart primary ext4 1M ${BOOT_SIZE}
  parted /dev/${DISK} mkpart primary ext4 ${BOOT_SIZE} 100%
  parted /dev/${DISK} set 1 boot on

  echo -n ${PASSWD_DISK} | cryptsetup luksFormat --type luks2 /dev/${DISK}2
  echo -n ${PASSWD_DISK} | cryptsetup open /dev/${DISK}2 cryptroot

  pvcreate /dev/mapper/cryptroot
  vgcreate vol /dev/mapper/cryptroot

  lvcreate -L ${SWAP_SIZE}G vol -n swap
  lvcreate -l +100%FREE vol -n root

  mkfs.fat -F32 /dev/${DISK}1
  mkswap /dev/mapper/vol-swap
  swapon /dev/mapper/vol-swap
  mkfs.ext4 /dev/mapper/vol-root

  mount /dev/mapper/vol-root /mnt
  mkdir -p /mnt/boot
  mount /dev/${DISK}1 /mnt/boot
}



tarzan=$(check_uefi_mode)
echo "$tarzan"

[ "$tarzan" == "y" ] && echo "siii" || echo "nnnnn"

select_disk
create_partitions
echo "el valor seleccionado es $DISK"



