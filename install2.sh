#!/bin/bash

####################################################################################################################
#
# Script de Instalación de Arch Linux
# Autor: tecbs
# Descripción: Este script automatiza la instalación de Arch Linux con particiones cifradas LUKS y UEFI.
# Uso: Ejecuta este script como superusuario en una consola de Arch Linux en modo UEFI. Hazlo bajo tu responsabilidad
#      y preferntemente en maquinas virtuales antes de hacerlo en ordenador físico.
#
######################################################################################################################

#variables
DISK=""
BOOT_SIZE="512M"
SWAP_SIZE="1"
PASSWD_DISK="aaa"
ZONEINFO_I="/Europe/Madrid"
LANG_I="es_ES.UTF-8"
LANG_TYPE="UTF-8"
KEYMAP_I="us-acentos"
PASSWD_DISK="aaa"
PASSWD_ROOT="aaa"
USERN="bir"
PASSWD_USERN="aaa"
LOCALHOST_I="Archlinux"
PACSTRAP_I="base linux linux-firmware lvm2 efibootmgr"
PACMAN_I="networkmanager grub neovim wget git sudo"
CRYPT="NO"
ROOTNAME="root"
###############################################################################################
# el disco está encriptado
###############################################################################################
crypt_disk() {
  # Pregunta al usuario y almacena la respuesta en la variable 'respuesta'
  read -p "¿Quieres que la instalción esté encritpada, contesta SI en mnayusculas o dale otra tecla? (SI/??): " respuesta
  # Verifica si la respuesta es "SI" en mayúsculas
  [ "$respuesta" == "SI" ] && CRYPT="$respuesta" || echo "No encriptadao"
  [ "$respuesta" == "SI" ] && ROOTNAME="cryptroot"
}
###############################################################################################
# manejar de errores
###############################################################################################
error_exit() {
  echo "Error: $1" >&2
  exit 1
}
###############################################################################################
# borra disco
###############################################################################################

delete_disk(){
blkdiscard /dev/${DISK}
}
###############################################################################################
# seleccionar el disco
###############################################################################################
select_disk() {
# Enumerar los discos existentes
available_disks=($(lsblk --noheadings --raw -d | grep 'disk' | awk '{print $1"("$4")"}'))

# Verificar si existen discos disponibles
if [ ${#available_disks[@]} -eq 0 ]; then
  echo "No se encontraron discos disponibles."
  exit 1
fi

# Agregar "Salir" como una opción adicional al final de la lista
available_disks+=("Exit/Salir")

# Mostrar un menú desplegable para seleccionar un disco
PS3="Por favor, elige un disco de la lista (o 'Salir' para salir): "
select selected_disk in "${available_disks[@]}"; do
  if [ "$selected_disk" = "Exit/Salir" ]; then
    echo "Saliendo del programa."
    exit 0
  elif [ -n "$selected_disk" ]; then
    DISK=$(echo "$selected_disk" | sed 's/([^)]*)//')
    echo "Has seleccionado el disco: $DISK"
    break
  else
    echo "Selección no válida. Por favor, elige un número de la lista o 'Salir' para salir."
  fi
done

}

###############################################################################################
# Chequea si es UEFI
###############################################################################################
check_uefi_mode() {
  [ -d "/sys/firmware/efi/efivars" ] && echo "y" || echo "n"
}

###############################################################################################
# Crea y monta particiones
###############################################################################################
create_partitions() {

  sgdisk --zap-all /dev/${DISK}

  parted /dev/${DISK} mklabel gpt
  parted /dev/${DISK} mkpart primary ext4 1M ${BOOT_SIZE}
  parted /dev/${DISK} mkpart primary ext4 ${BOOT_SIZE} 100%
  parted /dev/${DISK} set 1 boot on
  if [ "$CRYPT" = "SI" ]; then
  echo -n ${PASSWD_DISK} | cryptsetup luksFormat --type luks2 /dev/${DISK}2
  echo -n ${PASSWD_DISK} | cryptsetup open /dev/${DISK}2 cryptroot
  fi
  pvcreate /dev/mapper/${ROOTNAME}
  vgcreate vol /dev/mapper/${ROOTNAME}

  lvcreate -L ${SWAP_SIZE}G vol -n swap
  lvcreate -l +100%FREE vol -n root

  mkfs.fat -F32 /dev/${DISK}1
  mkswap /dev/mapper/vol-swap
  swapon /dev/mapper/vol-swap
  mkfs.ext4 /dev/mapper/vol-root

  mount /dev/mapper/vol-root /mnt
  mkdir -p /mnt/boot
  mount /dev/${DISK}1 /mnt/boot
}

###############################################################################################
# Instala pacstrap
###############################################################################################
install_pacstrap(){
# Configurar espejo de repositorios más rápido
echo "Server = http://mirrors.kernel.org/archlinux/\$repo/os/\$arch" >/etc/pacman.d/mirrorlist

# Instalar el sistema base
pacstrap /mnt ${PACSTRAP_I} --noconfirm --disable-download-timeout

}

###############################################################################################
# Establece la información de la zona
###############################################################################################
select_zoneinfo(){
arch-chroot /mnt /bin/bash -c "ln -sf /usr/share/zoneinfo${ZONEINFO_I} /etc/localtime"
}

###############################################################################################
# Establece el reloj
###############################################################################################
select_clock(){
arch-chroot /mnt /bin/bash -c "hwclock --systohc"
}

###############################################################################################
# Selecciona idioma y teclado
###############################################################################################
select_lang(){
arch-chroot /mnt /bin/bash -c "echo '${LANG_I} ${LANG_TYPE}' > /etc/locale.gen"
arch-chroot /mnt /bin/bash -c "locale-gen"
arch-chroot /mnt /bin/bash -c "echo 'LANG=${LANG_I}' > /etc/locale.conf"
arch-chroot /mnt /bin/bash -c "echo 'KEYMAP=${KEYMAP_I}' > /etc/vconsole.conf"

}

###############################################################################################
# Selecciona hostname y hosts
###############################################################################################
select_hosts(){
arch-chroot /mnt /bin/bash -c "echo ${LOCALHOST_I} > /etc/hostname"
arch-chroot /mnt /bin/bash -c "echo '127.0.0.1	${LOCALHOST_I}' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "echo '::1	localhost' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "echo '127.0.0.1	localhost' >> /etc/hosts"

}

###############################################################################################
# Modifica hooks de /etc/mkinitcpio
###############################################################################################
select_hooks(){
if [ "$CRYPT" = "SI" ]; then
arch-chroot /mnt /bin/bash -c "sed -i '/^HOOKS=/s/.*/HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 filesystems keyboard fsck)/' /etc/mkinitcpio.conf"
else
arch-chroot /mnt /bin/bash -c "sed -i '/^HOOKS=/s/.*/HOOKS=(base udev autodetect modconf block keymap lvm2 filesystems keyboard fsck)/' /etc/mkinitcpio.conf"
fi
}

###############################################################################################
# Instala las aplicaciones necesarias para el funcionamiento
###############################################################################################
install_apps(){
arch-chroot /mnt /bin/bash -c "pacman -S ${PACMAN_I} --noconfirm --disable-download-timeout"
}

###############################################################################################
# Configura Grub
###############################################################################################
configure_grub(){
arch-chroot /mnt /bin/bash -c "grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB"
if [ "$CRYPT" = "SI" ]; then
arch-chroot /mnt /bin/bash -c "sed -i 's/^GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"cryptdevice=\/dev\/${DISK}2:cryptroot root=\/dev\/mapper\/vol-root\"/' /etc/default/grub"
fi
arch-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
}

###############################################################################################
# Conecta el servicio de red
###############################################################################################
enable_network(){
arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager"
}

###############################################################################################
# Crea usuario con permisos sudo
###############################################################################################
create_user(){
arch-chroot /mnt /bin/bash -c "useradd -m ${USERN}"
arch-chroot /mnt /bin/bash -c "echo \"${USERN}:${PASSWD_USERN}\" | chpasswd"
arch-chroot /mnt /bin/bash -c "usermod -aG wheel ${USERN}"
arch-chroot /mnt /bin/bash -c "sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers"
}

###############################################################################################
# Establece contraseña de root
###############################################################################################
passwd_root(){
arch-chroot /mnt /bin/bash -c "echo \"root:${PASSWD_ROOT}\" | chpasswd"
}

###############################################################################################
# Programa main con manejo de errores
###############################################################################################
main() {
  crypt_disk || error_exit "Error al definir la encriptación"
  select_disk || error_exit "Error al seleccionar el disco."
  delete_disk || error_exit "Error borrando disco"
  create_partitions || error_exit "Error al crear particiones."
  install_pacstrap || error_exit "Error al instalar el sistema base."
  select_zoneinfo || error_exit "Error al configurar la zona horaria."
  select_clock || error_exit "Error al configurar el reloj del sistema."
  select_lang || error_exit "Error al configurar el idioma."
  select_hosts || error_exit "Error al configurar el archivo /etc/hosts."
  select_hooks || error_exit "Error al configurar los hooks de initramfs."
  install_apps || error_exit "Error al instalar paquetes adicionales."
  configure_grub || error_exit "Error al instalar GRUB."
  enable_network || error_exit "Error al habilitar NetworkManager."
  create_user || error_exit "Error al crear el usuario."
  passwd_root || error_exit "Error al configurar la contraseña de root."
}

###############################################################################################
# Lanza programa main
###############################################################################################
main

