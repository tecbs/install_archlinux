#!/bin/bash

USERN="bir"
# Cambiar la raíz al nuevo sistema
arch-chroot /mnt /bin/bash -c "ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime"
arch-chroot /mnt /bin/bash -c "hwclock --systohc"
arch-chroot /mnt /bin/bash -c "echo 'es_ES.UTF-8 UTF-8' > /etc/locale.gen"
arch-chroot /mnt /bin/bash -c "locale-gen"
arch-chroot /mnt /bin/bash -c "echo 'LANG=es_ES.UTF-8' > /etc/locale.conf"
arch-chroot /mnt /bin/bash -c "echo 'KEYMAP=es' > /etc/vconsole.conf"
arch-chroot /mnt /bin/bash -c "echo 'mi-arch' > /etc/hostname"
arch-chroot /mnt /bin/bash -c "echo -n 'root:aaa' | chpasswd"
arch-chroot /mnt /bin/bash -c "echo '127.0.0.1	mi-arch' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "echo '::1	localhost' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "echo '127.0.0.1	localhost' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "sed -i '/^HOOKS=/s/.*/HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 filesystems keyboard fsck)/' /etc/mkinitcpio.conf"
arch-chroot /mnt /bin/bash -c "mkinitcpio -p linux"
#GRUB y configurarlo
arch-chroot /mnt /bin/bash -c "pacman -S networkmanager grub neovim wget git sudo --noconfirm"
#arch-chroot /mnt /bin/bash -c "sed -i '/^GRUB_ENABLE_CRYPTODISK/s/.*/GRUB_ENABLE_CRYPTODISK=y' /etc/default/grub"
#arch-chroot /mnt /bin/bash -c "sed -i '/^GRUB_CMDLINE_LINUX_DEFAULT=/s/.*/GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=3 resume=/dev/mapper/vol-swap\"/' /etc/default/grub"
arch-chroot /mnt /bin/bash -c "grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB"
arch-chroot /mnt /bin/bash -c "sed -i 's/^GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"cryptdevice=\/dev\/vda2:cryptroot root=\/dev\/mapper\/vol-root\"/' /etc/default/grub"
arch-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager"
arch-chroot /mnt /bin/bash -c "echo \"root:aaa\" | chpasswd"
arch-chroot /mnt /bin/bash -c "useradd -m ${USERN}"
arch-chroot /mnt /bin/bash -c "echo \"${USERN}:aaa\" | chpasswd"
arch-chroot /mnt /bin/bash -c "usermod -aG wheel ${USERN}"
arch-chroot /mnt /bin/bash -c "sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers"

