#!/bin/bash

# Verificar si se está ejecutando como root
if [ "$EUID" -ne 0 ]; then
	echo "Este script debe ejecutarse como root."
	exit 1
fi

# Asegurarse de que el disco esté desmontado
umount -R /mnt 

# Variables de particionado
DISK="/dev/vda"
BOOT_SIZE="512M"
SWAP_SIZE="1"
PASWD_DISK="aaa"
PASWD_ROOT="aaa"
USERN="bir"
PASSWD_USERN="aaa"

# Limpiar el disco
sgdisk --zap-all ${DISK}

# Crear particiones
sudo parted ${DISK} mklabel gpt
sudo parted ${DISK} mkpart primary ext4 1M ${BOOT_SIZE}
sudo parted ${DISK} mkpart primary ext4 ${BOOT_SIZE} 100%
#parted ${DISK} mkpart primary linux-swap 15GiB 16Gi
parted ${DISK} set 1 boot on

# Formatear las particiones y configurar la encriptación
echo -n ${PASWD_DISK} | cryptsetup luksFormat --type luks2 ${DISK}2
echo -n ${PASWD_DISK} | cryptsetup open ${DISK}2 cryptroot

#creo la unidad
pvcreate /dev/mapper/cryptroot

#creo volumen fisico
vgcreate vol /dev/mapper/cryptroot

#creo particiones

lvcreate -L ${SWAP_SIZE}G vol -n swap
lvcreate -l +100%FREE vol -n root


#formatear la particion boot
mkfs.fat -F32 ${DISK}1

#formateamos swap
mkswap /dev/mapper/vol-swap
swapon /dev/mapper/vol-swap

#formateamos la raiz
mkfs.ext4 /dev/mapper/vol-root

# Montar la partición raíz
mount /dev/mapper/vol-root /mnt

# Montar la partición EFI
mkdir -p /mnt/boot
mount ${DISK}1 /mnt/boot # Reemplaza ${DISK}3 con tu partición EFI real

# Configurar espejo de repositorios más rápido
echo "Server = http://mirrors.kernel.org/archlinux/\$repo/os/\$arch" >/etc/pacman.d/mirrorlist

# Instalar el sistema base
pacstrap /mnt base linux linux-firmware lvm2 efibootmgr --noconfirm --disable-download-timeout

# Generar el archivo fstab
genfstab -U /mnt >>/mnt/etc/fstab

# Cambiar la raíz al nuevo sistema
arch-chroot /mnt /bin/bash -c "ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime"
arch-chroot /mnt /bin/bash -c "hwclock --systohc"
arch-chroot /mnt /bin/bash -c "echo 'es_ES.UTF-8 UTF-8' > /etc/locale.gen"
arch-chroot /mnt /bin/bash -c "locale-gen"
arch-chroot /mnt /bin/bash -c "echo 'LANG=es_ES.UTF-8' > /etc/locale.conf"
arch-chroot /mnt /bin/bash -c "echo 'KEYMAP=es' > /etc/vconsole.conf"
arch-chroot /mnt /bin/bash -c "echo 'mi-arch' > /etc/hostname"
arch-chroot /mnt /bin/bash -c "echo -n 'root:aaa' | chpasswd"
arch-chroot /mnt /bin/bash -c "echo '127.0.0.1	mi-arch' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "echo '::1	localhost' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "echo '127.0.0.1	localhost' >> /etc/hosts"
arch-chroot /mnt /bin/bash -c "sed -i '/^HOOKS=/s/.*/HOOKS=(base udev autodetect modconf block keymap encrypt lvm2 filesystems keyboard fsck)/' /etc/mkinitcpio.conf"
arch-chroot /mnt /bin/bash -c "mkinitcpio -p linux"
#GRUB y configurarlo
arch-chroot /mnt /bin/bash -c "pacman -S networkmanager grub neovim wget git sudo --noconfirm --disable-download-timeout"
#arch-chroot /mnt /bin/bash -c "sed -i '/^GRUB_ENABLE_CRYPTODISK/s/.*/GRUB_ENABLE_CRYPTODISK=y' /etc/default/grub"
#arch-chroot /mnt /bin/bash -c "sed -i '/^GRUB_CMDLINE_LINUX_DEFAULT=/s/.*/GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=3 resume=/dev/mapper/vol-swap\"/' /etc/default/grub"
arch-chroot /mnt /bin/bash -c "grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB"
arch-chroot /mnt /bin/bash -c "sed -i 's/^GRUB_CMDLINE_LINUX=.*/GRUB_CMDLINE_LINUX=\"cryptdevice=${DISK}2:cryptroot root=\/dev\/mapper\/vol-root\"/' /etc/default/grub"
arch-chroot /mnt /bin/bash -c "grub-mkconfig -o /boot/grub/grub.cfg"
arch-chroot /mnt /bin/bash -c "systemctl enable NetworkManager"
arch-chroot /mnt /bin/bash -c "echo \"root:aaa\" | chpasswd"
arch-chroot /mnt /bin/bash -c "useradd -m ${USERN}"
arch-chroot /mnt /bin/bash -c "echo \"${USERN}:aaa\" | chpasswd"
arch-chroot /mnt /bin/bash -c "usermod -aG wheel ${USERN}"
arch-chroot /mnt /bin/bash -c "sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers"




